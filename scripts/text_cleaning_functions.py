import re

import nltk
import pandas as pd
from nltk import WordNetLemmatizer
from nltk.corpus import stopwords
from string import ascii_uppercase, ascii_lowercase


def remove_pattern(series: pd.Series, pattern: str) -> pd.Series:
    return series.str.replace(pattern, '', regex=True)


def remove_whitespaces(series: pd.Series) -> pd.Series:
    temp = series.str.strip()
    return temp.str.strip(r'\\r')


def drop_line_by_pattern(df: pd.DataFrame, column_name: str, pattern: str, present=True) -> pd.DataFrame:
    temp = df.drop(
        df[
            df[column_name].str.contains(pattern, regex=True) == present
            ].index
    )
    return temp


# Deeper cleaning.
def remove_apostrophe(arr: list):
    arr = [el.replace('’', '\'') for el in arr]
    return arr


def remove_nums(arr):
    pattern = '[0-9]'
    arr = [re.sub(pattern, '', i) for i in arr]
    return arr


def apply_stemmer(arr: list, language='english'):
    stemmer = WordNetLemmatizer()
    stop_words = stopwords.words(language)
    if language == 'english':
        stop_words += ['im', 'i\'m', 'mr', 'hes', 'myan', 'thats', 'isi', 'like', 'actually', 'rep', 'gop', 'daily']
    elif language == 'german':
        stop_words += ['mio', 'mlrd', 'zugleich', 'on', 'of']
    filtered_text = [stemmer.lemmatize(word) for word in arr if word.lower() not in stop_words]
    return filtered_text


def remove_quotes(arr: list):
    return [el.replace('\'', '') for el in arr]


# Additional features.

def count_subsequent_uppercase_letters(title: str) -> int:
    total = 0
    counter = 0
    for letter in title:
        if letter in ascii_uppercase:
            counter += 1
        elif letter in ascii_lowercase:
            total = max(total, counter)
            counter = 0
    return max(total, counter)


def confirm_link_presence(string: str) -> int:
    pattern = r'\s*https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*\s*$)'
    return 1 if len(re.findall(pattern, string)) > 0 else 0


def count_dogs(string: str) -> int:
    return string.count('@')


def count_emotional_signs(string: str) -> int:
    return string.count("!") + string.count("?")


def count_quotes(string: str) -> int:
    return string.count('"') + string.count('“') + string.count('”') + string.count('„')
