# Projekt 1: Fake-News-Detection.

## Aufgabe 1 (Einrichtung Azure).
- [x] Erstellen Sie einen Azure Account.

## Aufgabe 2 (Datensatz vorbereiten).
- [x] Den Datensatz `news1.zip` herunterladen.
- [x] Daten anpassen, damit ein passendes Modell trainiert werden kann.
- [x] Den angepassten Datensatz in Azure hochladen.

## Aufgabe 3 (Training).
- [x] Den Datensatz in Trainings- und Testdaten aufteilen. 
- [x] Machine Learning Studio nutzen, um ein passendes Modell zu trainieren.

## Aufgabe 4 (Evaluierung).
- [x] Den Testdatensatz benutzen, um relevante Metriken herauszufinden.
- [x] Was fällt auf? 
- [x] Wie sind die Ergebnisse zu bewerten?
- [x] Muss der Datensatz eventuell angepasst werden?

## Aufgabe 5 (Fine Tuning).
- [x] Kann Ihr Modell noch verbessert werden?
- [x] Geeignete Verfahren finden und Hyperparameter anpassen.
- [x] Training und Evaluierung wiederholen.
- [x] Wie verändert sich das Modell?

## Aufgabe 6 (Neuer Datensatz). 
- [x] Die selben Schritte für den Datensatz `news2.zip` durchführen.
- [x] Vorbereitung des Datensatzes und getroffenen Entscheidungen in der Präsentation und Dokumentation ausführlich erklären.

## Aufgabe 8 (Händische Entwicklung) - Optional.
- [x] Zu den beiden Datensätzen jeweils ein Modell trainieren.
- [x] Wie lässt sich der Trainingsprozess der verschiedenen Plattformen vergleichen?
- [x] Sind die Ergebnisse gleich?
